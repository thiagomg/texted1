# Repository deprecation notice

This repository is **deprecated**. Please use the rewritten next generation hosted here:

https://gitlab.com/thiagomg/texted

---

# TexteD - Blog system, markdown based


Before compiling
* Get [DMD compiler](https://dlang.org/download.html)
* Get [DUB](https://code.dlang.org/download)

# Setting up your environment.

Simply run 

```
setup.sh
```

# Running the website for debugging and testing

Just run

```
dub
```

# There's a convinience script for release builds

```
compile.sh
```