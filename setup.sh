#!/bin/sh

mkdir comments
mkdir data
mkdir posts
mkdir public
mkdir template

echo "<html><head><title>My blog</title></head>
<body>
    <h1>List of posts</h1>
    {{#post_list}}
        <div>
        {{date}} {{time}}
        <h3><a href='{{file_name}}'>{{title}}</a></h3>
        <span>{{summary}}</span>
        <hr>
        </div>
    {{/post_list}}
</body>
</html>" > template/postlist.tpl

echo "<html><head><title>My blog</title></head>
<body>
    <a href='/'>home</a>
    <h1>Single post view</h1>
        {{#errors}}
        <p><font color="red">Error - {{error}}</font></p>
        {{/errors}}

        Created by {{author}} on {{date}} {{time}}

        {{post_content}}

        <h2 id="comment">Tell me your opinion!</h2>

        {{#can_comment}}
        <form method="post" action="/comment/{{id}}">
            Name<br />
            <input type="text" size="40" maxlength="64" name="name" value=""><br />
            E-mail<br />
            <input type="text" size="40" maxlength="64" name="email" value=""><br />
            Comment<br/>
            <textarea rows="8" cols="50" maxlength="512" name="comment"></textarea>
            <br />
            <input type="submit" value="Send comment">
        </form>
        {{/can_comment}}
        {{#maybe_robot}}
          <form method='POST' action='/human/verify'><br>
              {{#maybe_robot_error}}
              <strong>Error! Please try again. <br />
                You already tried {{robot_tries}} times</strong><br /><br />
              {{/maybe_robot_error}}

              Please prove you are human.<br />
              What is the result of {{question}} ? <br /><br />
              <input type='text' size="4" name='q_answer'>
              <input type='submit' value="Verify"><br />
            </div>
          </form>
        {{/maybe_robot}}

        <h2>Other comments</h2>
        {{#comments}}
        Auhor: {{author}}<br />
        Date: {{date}} {{time}}<br />
        <span>{{text}}</span><br />
        {{/comments}}
        {{^comments}}
        No comments !
        {{/comments}}

    </div>
</body>
</html>" > template/view.tpl

echo "[ID]: # (665f0591-a673-4248-b112-3e518142c857)
[DATE]: # (2017-12-24 10:49:58.103)
[AUTHOR]: # (my-name)
# Welcome!

This is a sample post. You are setup" > posts/sample.md

