import vibe.d;

import texted.http_handler;
import texted.config_holder;
import config.reader;

import std.stdio;

shared static this()
{
    writeln("Starting thiagocafe");

	auto settings = new HTTPServerSettings;

    settings.port = to!short(get_config().get!long("tcp.bind.port"));
    settings.bindAddresses = get_config().get_or!(string[])("tcp.bind.address", [""]);
	settings.sessionStore = new MemorySessionStore;

    listenHTTP(settings, &handler);

	logInfo("Thiago Cafe v1.1 running in %s : %d", settings.bindAddresses, settings.port);
	logInfo("Domains: %s", app_cfg().domains);
}
