module config.parser;

import std.stdio, std.array, std.algorithm;
import std.string, std.ascii;
import std.typecons, std.conv;
import std.variant;

/**
 * returns k, v from the string formatted as k=v
 * if format not found, return an empty ["",""]
 */
auto get_kv(string line) {
    auto kv = line.split('=');
    if(kv.length != 2) {
        return ["",""];
    }
    return [kv[0].strip, kv[1].strip];
}

auto _line_item(long pos, string line) {
    return tuple!("next","str")(pos, line);
}

long search_comment(string line, long start) {
    for(; start < line.length; ++start) {
        if(!line[start].isWhite)
            break;
    }

    auto slice = line[start..$];

    //let's check single line comment
    if(slice.startsWith("#") || slice.startsWith("//")) {
        for(; start < line.length; ++start) {
            if( line[start] == '\r' || line[start] == '\n' ) {
                if(start+1 < line.length && (line[start+1] == '\r' || line[start+1] == '\n')) {
                    ++start;
                }
                return start;
            }
        }
    }

    //and now multi-line comment
    string comment_end = "";
    if(slice.startsWith("/*")) {
        comment_end = "*/";
    } else if(slice.startsWith("(*")) {
        comment_end = "*)";
    }

    if(!comment_end.empty) {
        //Let's search for the end comment token
        long pos = slice.indexOf(comment_end);
        if(pos >= 0) {
            return start+pos+2; //Let's account for the token size
        }
    }
    return 0;
}

auto get_single_line(string line, long start=0) {
    if(start < 0)
        return _line_item(-1L, "");

    //Let's search for comments
    long comment_pos = search_comment(line, start);
    if( comment_pos > 0 ) {
        return _line_item(comment_pos, "");
    }

    long end = line.indexOf(';', start);
    if( end == -1 ) {
        //not found!
        return _line_item(-1L, "");
    }
    return _line_item(end+1, line[start..end]);
}

enum TYPE {
    EMPTY, COMMENT, STRING, NUMERIC, LIST
}

auto _valtype(Range)(TYPE typename, Range value) {
    return tuple!("type","value")(typename,value);
}

auto get_string(Range)(Range value) {
    auto s = value.strip;
    if( (s.startsWith('"') || s.startsWith('\'')) &&
        s.endsWith(s[0]) ) {
        return s[1..$-1];
    }
    return s;
}

auto get_value_type(Range)(Range value) {
    long start=0;
    for(; start < value.length; ++start) {
        if(value[start].isWhite) continue;
        break;
    }
    if( start == value.length )
        return _valtype(TYPE.EMPTY, "");

    long end = value.length-1;
    for(; end > start; --end) {
        if(value[end].isWhite) continue;
        break;
    }
    ++end; //we stop when reaching a non-white char.

    auto s = value[start..end];
    if(s.startsWith("#") || s.startsWith("//")) {
        return _valtype(TYPE.COMMENT, s);
    }
    if(s.startsWith("\"") || s.startsWith("'")) {
        return _valtype(TYPE.STRING, get_string(s));
    }
    if(s.startsWith("[")) {
        if(s.endsWith("]"))
            return _valtype(TYPE.LIST, s[1..$-1]);
        return _valtype(TYPE.STRING, s);
    }

    if(s.isNumeric) {
        return _valtype(TYPE.NUMERIC, s);
    }
    return _valtype(TYPE.STRING, s);
}

bool hasDecimal(string r) {
    if( r.indexOf('.') != -1 ) {
        return true;
    }
    return false;
}

Variant to_variant(Range)(Range item) {
    Variant ret; //please remove
    switch( item.type ) {
    case TYPE.STRING:
        ret = item.value;
        break;
    case TYPE.NUMERIC:
        //long / real
        if( hasDecimal(item.value) ) {
            ret = to!double(item.value);
        } else {
            ret = to!long(item.value);
        }
        break;
    case TYPE.LIST:
        string[] list = item.value
            .split(",")
            .map!(s => s.get_string)
            .array;

        int ints = 0;
        int decs = 0;
        foreach(string s; list) {
            if( s.isNumeric ) {
                if( s.hasDecimal ) ++decs;
                else ++ints;
            }
        }

        if( ints == list.length ) {
            ret = map!(s => to!long(s))(list).array;
        } else if( (decs+ints) == list.length ) {
            ret = map!(s => to!double(s))(list).array;
        } else {
            ret = list.array;
        }
        break;
    default:
        ret = "";
    }
    return ret;
}

/**
 * structure of a line:
 * #, // -> single line comment
 * key.subkey.subsubkey = value;
 */
void parse_string(string line, ref Variant[string] config) {
    //TODO: What to do if we receive more than one line ?
    auto item = get_single_line(line);

    while(item.next != -1) {
        if(!item.str.empty) {
            string[] kv = get_kv(item.str);

            //parse value and store
            string s_value = kv[1];
            auto item_value = get_value_type(s_value);
            if( item_value.type == TYPE.COMMENT || item_value.type == TYPE.EMPTY ) {
                continue;
            }
            //writeln(item_value.type, " --> ", item_value);
            config[kv[0]] = to_variant(item_value);
        }
        item = get_single_line(line, item.next);
    }
}

//let's ensure get_kv return correct values
unittest {
    assert(get_kv("a=b") == ["a","b"]);
    assert(get_kv("a-b") == ["",""]);
    assert(get_kv("") == ["",""]);
}

//get_value_type unit tests
unittest {
    assert(get_value_type("").type == TYPE.EMPTY);
    assert(get_value_type(" ").type == TYPE.EMPTY);
    assert(get_value_type(" --=-=- ").type == TYPE.STRING);
    assert(get_value_type("asd  ").type == TYPE.STRING);
    assert(get_value_type("\"asd\"  ").type == TYPE.STRING);
    assert(get_value_type("'asd'").type == TYPE.STRING);
    assert(get_value_type("1980").type == TYPE.NUMERIC);
    assert(get_value_type("['asdasd',1]").type == TYPE.LIST);
    assert(get_value_type(" //sadsadasd").type == TYPE.COMMENT);
    assert(get_value_type(" #sadsadasd").type == TYPE.COMMENT);
    assert(get_value_type(" asd").type == TYPE.STRING);
    assert(get_value_type(" \"asd\" ").type == TYPE.STRING);
    assert(get_value_type(" 'asd'").type == TYPE.STRING);
    assert(get_value_type(" ['asdasd',1]  ").type == TYPE.LIST);
    assert(get_value_type(" 2008.2010  ").type == TYPE.NUMERIC);
    assert(get_value_type(" /asd/dfg/g  ").type == TYPE.STRING);
    assert(get_value_type(" \"sads  ").type == TYPE.STRING);
    assert(get_value_type(" [sad,d  ").type == TYPE.STRING);
}

unittest {
    auto tv(string s) { return to_variant(get_value_type(s)); }
    assert( tv("val") == Variant("val") );
    assert( tv("'val'") == Variant("val") );
    assert( tv("1")  == Variant(1L));
    assert( tv("1.0")  == Variant(1.0) );
    assert( tv("[ a ,b ,c, d]") == Variant(["a","b","c","d"]) );
    assert( tv("[\" a \",b , c,' d']") == Variant([" a ","b","c"," d"]) );
    assert( tv("[ 1, 2 ,3, 4]") == Variant([1L,2,3,4]) );
    assert( tv("[ 1.0, 2.0 ,3.1, 4.0]") == Variant([1,2,3.1,4]) );
    assert( tv("[ 1, 2 ,3.2, 4]") == Variant([1,2,3.2,4]) );
    assert( tv("[ 1, 2 ,3.3, 4, a]") == Variant(["1","2","3.3","4","a"]) );
}
