module config.reader;

import std.file;
import std.variant;
import std.stdio;
import std.algorithm;
import std.array;

import config.parser;

alias config_data = Variant[string];

Variant get_or_variant(config_data config, string key, Variant def_value) {
    if( key in config ) {
        return config[key];
    }
    return def_value;
}

Type get_or(Type)(config_data config, string key, Type def_value) {
    return get_or_variant(config, key, Variant(def_value)).get!Type;
}

Type get(Type)(config_data config, string key) {
    //FIXME: If Type is array, like string[], instantiate properly
    return get_or!Type(config, key, Type());
}

string[] get(Type : string[])(config_data config, string key) {
    return get_or!(string[])(config, key, [""]);
}
string get(Type : string)(config_data config, string key) {
    return get_or!string(config, key, "");
}
string get_s(config_data config, string key) {
    return get!(string)(config, key);
}

string[] prefix(config_data config, string key_prefix) {
    string [] ret;
    auto app = appender(&ret);
    foreach(string key; config.byKey) {
        if( key.startsWith(key_prefix) ) {
            app ~= key;
        }
    }
    return ret;
}

config_data from_prefix(config_data config, string key_prefix) {
    config_data ret;
    foreach(string key; config.byKey) {
        if( key.startsWith(key_prefix) ) {
            ret[key] = config[key];
        }
    }
    return ret;
}

void read_config(ref config_data config, string file_name) {
    file_name.readText.parse_string(config);
}
