module texted.utils;

import std.stdio;
import std.typecons;
import std.string;

void log(T)(T obj) {
  static if (is(T == struct) || is(T == class)){
     writef("{");
     foreach(i,_;obj.tupleof) {
       writefln("%s : %s,", obj.tupleof[i].stringof[4..$], obj.tupleof[i]);
     }
     writefln("}");
  }
  else {
     writefln(obj);
  }
}


bool is_metadata(char [] line) {
    import std.regex;

    static r = regex("^\\[.+\\]: # (.+)");
    auto ret = match(line, r);

    return !(ret.empty);
}

auto get_pair(char f, char l, char [] line, long start) {
    auto ret = tuple(0L, 0L);
    long ini = indexOf(line, f, start);
    if( ini == -1 ) return ret;
    long end = indexOf(line, l, ini);
    if( end == -1 ) return ret;
    return tuple(ini+1, end); //[)
}

auto is_empty(Tuple!(long, long) t) {
    return ( t[0] == 0 && t[1] == 0 );
}

auto get_metadata(char [] line) {
    if( is_metadata(line) ) {
        auto p1 = get_pair('[', ']', line, 0L);
        string k, v;
        if( !p1.is_empty ) {
            k = line[p1[0] .. p1[1]].dup;
        }
        auto p2 = get_pair('(', ')', line, p1[1]);
        if( !p2.is_empty ) {
            v = line[p2[0] .. p2[1]].dup;
        }
        return tuple(k, v);
    } else {
        return tuple("", "");
    }
}

Tuple!(string, string) extract_date_time(string utc_date) {
    
    if( utc_date.length == 26 || utc_date.length == 23 ) { //UTC standard || Int std
        return tuple(utc_date[0..10], utc_date[11..19]);
    } else if( utc_date.length >= 10 ) {
        return tuple(utc_date[0..10], "");
    }

    return tuple(utc_date, "");
}

string file_extension(string file_name) {
    long pos = file_name.lastIndexOf('.');
    if( pos == -1 )
        return "";
    return file_name[pos+1..$];
}

