module texted.stats;

import std.datetime;
import std.file;
import std.format;

import texted.config_holder;

void add_view(string path, string refer, string client_addr, SysTime date_time) {

    string record = date_time.toISOExtString() ~ "|" ~ path ~ "|" ~ client_addr ~ "|" ~ refer ~ "\n";

    auto tm = Clock.currTime();
    auto file_suffix = format("%04d%02d%02d", tm.year, tm.month, tm.day);

    synchronized {
        //FIXME: Create directory if it does not exist
        append(app_cfg().dir_usage_data ~ "stats_" ~ file_suffix ~ ".log", record);
    }

}
