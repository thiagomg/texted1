module texted.human;

import vibe.d;
import vibe.utils.dictionarylist;

import std.stdio;
import std.file;
import std.typecons;

import texted.utils;
import texted.number;

//TODO: Move it out of here -----------------
auto current_session(HTTPServerRequest req, HTTPServerResponse res) {
    if (req.session)
        return req.session;
    res.terminateSession();
    return res.startSession();
}

string _get(Session session, string key, string def="") {
    if (session.isKeySet(key)) {
        return session.get!string(key);
    }
    return def;
}

bool _get_b(Session session, string key) {
    if (session.isKeySet(key)) {
        return session.get!bool(key);
    }
    return false;
}
//--------------------------------------------

void human(string [] params, HTTPServerRequest req, HTTPServerResponse res) {

    auto session = current_session(req, res);
    bool human_value = _get_b(session, "is_human");

    if( params.length > 0 && params[0] == "ask" ) {
        ask_question(req, res);
        return;
    } else if( params.length > 0 && params[0] == "verify" ) {
        verify(req, res);
        return;
    }

    record_question(session, req, res);

    string body;
    body ~= "<html><body>";
    body ~= "Is human: " ~ (human_value?"T":"F") ~ "<br>";
    body ~= "Q: " ~ _get(session, "question") ~ "<br>";
    body ~= "R: " ~ _get(session, "response") ~ "<br>";
    body ~= "<form method='POST' action='/human/verify'>" ~ "<br>";
    body ~= "<input type='text' name='q_answer'>" ~ "<br>";
    body ~= "<input type='submit'>" ~ "<br>";
    body ~= "</form>";
    body ~= "</body></html>";

    res.writeBody(body, "text/html; charset=UTF-8");
}

void verify(HTTPServerRequest req, HTTPServerResponse res) {

    auto session = current_session(req, res);
    bool is_human = false;
    string url_return = _get(session, "url_return");

    if( "q_answer" in req.form ) {
        string answer = req.form["q_answer"];

        if(answer == _get(session, "response")) {
            is_human = true;
        }
    }

    session.set("is_human", is_human);
    if(!is_human) {
        string tries_s = _get(session, "robot_tries", "0");
        int tries = to!int(tries_s);
        session.set("robot_tries", to!string(tries+1));
    }

    if(url_return != "") {
        res.redirect(url_return);
    } else {
        res.writeBody( (is_human ? "TRUE" : "FALSE"), "text/html; charset=UTF-8");
    }
}

void record_question(Session session, HTTPServerRequest req, HTTPServerResponse res) {
    auto calc = get_calc();
    string question = calc.sentence;
    string response = to!string(calc.result);
    session.set("question", question);
    session.set("response", response);
}

void ask_question(HTTPServerRequest req, HTTPServerResponse res) {
    auto session = current_session(req, res);

    record_question(session, req, res);

    string body = session._get("question");
    res.writeBody(body, "text/html; charset=UTF-8");
}

