module texted.comment;

import vibe.d;
import std.stdio;
import std.file;
import std.typecons;
import std.uuid;

import vibe.utils.dictionarylist;

//TODO: Move it out of here -----------------
auto current_session(HTTPServerRequest req, HTTPServerResponse res) {
    if (req.session)
        return req.session;
    return res.startSession();
}


bool _get_b(Session session, string key) {
    if (session.isKeySet(key)) {
        return session.get!bool(key);
    }
    return false;
}
//-------------------------------------------

void comment(string base_dir, string [] params, HTTPServerRequest req, HTTPServerResponse res) {

    auto session = current_session(req, res);
    bool human = _get_b(session, "is_human");
    if( !human ) {
        res.writeBody("Please prove you're a human", "text/html; charset=UTF-8");
        return;
    }

    string post_id = params[0];
    string referer = params[1];
    string comment_id = randomUUID().toString();
    string author_name = req.form["name"];
    string author_email = req.form["email"];
    string date_time = Clock.currTime().toISOExtString();
    string text = req.form["comment"];

    long idx = referer.indexOf("?");
    if( idx > -1 ) {
        referer = referer[0..idx];
    }

    if( author_name.length == 0 || author_email.length == 0 || text.length == 0 ) {
        string error = "?error=All fields are required";
        res.redirect(referer ~ error);
        return;
    }

    //TODO: Improve it.
    if( author_name.length > 64 ) {
        author_name = author_name[0..64];
    }
    if( author_email.length > 64 ) {
        author_email = author_email[0..64];
    }
    if( text.length > 512 ) {
        text = text[0..512];
    }

    string s;
    s ~= "ID: " ~ post_id ~ "<br />";
    s ~= "comment: " ~ text ~ "<br />";

    //Write directory creation
    string comment_dir = base_dir ~ post_id ~ "/";
    if( !comment_dir.exists ) {
        comment_dir.mkdir;
    }

    //Write comment file creation/append
    string buffer;
    buffer ~= "[ID]: # (" ~ comment_id ~ ")\n";
    buffer ~= "[AUTHOR]: # (" ~ author_name ~ ")\n";
    buffer ~= "[EMAIL]: # (" ~ author_email ~ ")\n";
    buffer ~= "[DATE]: # (" ~ date_time ~ ")\n";
    buffer ~= text ~ "\n\n";

    //locked!
    write_comment(comment_dir, buffer);

    res.redirect(referer);

}

string format_file_name(string comment_dir, long current_time) {
    return comment_dir ~ to!string(current_time) ~ ".cmt";
}

void write_comment(string comment_dir, string buffer) {
    synchronized {
        long current_time = Clock.currStdTime.stdTimeToUnixTime;

        string file_name = format_file_name(comment_dir, current_time);
        while( file_name.exists ) {
            ++current_time;
            file_name = format_file_name(comment_dir, current_time);
        }
        std.file.write(file_name, buffer);
    }
}
