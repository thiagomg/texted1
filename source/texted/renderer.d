module texted.renderer;

import vibe.d;
import vibe.utils.dictionarylist;

import mustache;

alias MustacheEngine!(string) Mustache;

import std.stdio;
import std.file;
import std.typecons;

import texted.utils;
import texted.human;
import texted.config_holder;

alias post_type = Tuple!(string, "buffer", string[string], "metadata");

struct post_item {
    string title;
    string file_name;
    string id;
    string date;
    string summary;
};

//TODO: Move it to configuration file
static immutable string[string] mime_types;

static this() {
    mime_types = [
        "gif": "image/gif",
        "jpeg": "image/jpeg",
        "jpg": "image/jpeg",
        "png": "image/png",
        "svg": "image/svg+xml"
    ];
}

Tuple!(string, bool) get_file(string base_dir, string name) {
    string file_name = base_dir ~ name ~ ".md";

    if( ! file_name.exists ) {
        //maybe changed the date. let's find
        auto pattern = "*" ~ name ~ ".md";
        auto files = dirEntries(base_dir, pattern, SpanMode.depth);
        foreach(f; files) {
            return tuple(f.name, true);
        }
    }

    return tuple(file_name, false);
}

auto get_content(string file_name) {

    string buffer;
    string[string] metadata;

    auto range = File(file_name).byLine();
    foreach (line; range)
    {
        auto ret = get_metadata(line);
        if( ret[0] == "" )
            buffer ~= line ~ "\n";
        else {
            metadata[ret[0]] = ret[1];
        }
    }
    return tuple(buffer, metadata);
}

void view(string base_dir, string [] params, string comment_dir, HTTPServerRequest req, HTTPServerResponse res) {
    //TODO: normalize. only A-Za-z_
    Tuple!(string, bool) post_file = get_file(base_dir, params[0]);
    string file_name = post_file[0];

    //is it redirect ?
    if( post_file[1] ) {
        // FIXME: view_pattern
        res.redirect("/" ~ app_cfg().view_pattern ~ "/" ~ file_name[ base_dir.length .. $-3 ] );
    }

    Mustache mustache;
    mustache.ext("tpl");
    auto context = new Mustache.Context;

    if( ! file_name.exists ) {
        string error = (app_cfg().dir_template ~ "error.html").readText;
        res.writeBody(error, "text/html; charset=UTF-8");
		return;
    }

    post_type post = get_content(file_name);

    string md_buffer = render_md(post.buffer);

    auto date_time = extract_date_time(post.metadata["DATE"]);

    if( "error" in req.query ) {
        auto sub = context.addSubContext("errors");
        sub["error"] = req.query["error"];
    }
    context["id"] = post.metadata["ID"];
    context["date"] = date_time[0];
    context["time"] = date_time[1];
    context["author"]  = post.metadata["AUTHOR"];
    context["post_content"]  = md_buffer;

    render_comment_box(req, res, context);

    //Ordering from the newest to the oldest
    render_comments(comment_dir, post.metadata["ID"], context);

    string page_content = mustache.render(app_cfg().dir_template ~ "view", context);

    res.writeBody(page_content, "text/html; charset=UTF-8");
}

void render_comment_box(HTTPServerRequest req, HTTPServerResponse res, Mustache.Context context) {

    auto session = current_session(req, res);
    bool human = _get_b(session, "is_human");

    if( human ) {
        context.addSubContext("can_comment");
    } else {
        auto sub = context.addSubContext("maybe_robot");
        record_question(session, req, res);
        sub["question"] = session._get("question");

        string tries = session._get("robot_tries", "0");
        if(tries != "0") {
            auto error_box = context.addSubContext("maybe_robot_error");
            error_box["robot_tries"] = tries;
        }

        string url_return = req.requestURI;
        if(url_return.find("#comment").empty)
            url_return ~= "#comment";
        session.set("url_return", url_return);
    }
}

// void list_comments(string comment_dir) {
//
//     auto dir_list = curdir.dirEntries(SpanMode.shallow);
//     sort!("a.name > b.name")(dir_list);
//
//     foreach(DirEntry dir; dir_list) {
//
//     }
//
// }

void render_comments(string comment_dir, string post_id, Mustache.Context context) {
    //Check against \[.+\]: #
    string curdir = comment_dir ~ post_id;
    if( !curdir.exists ) {
        return;
    }
    auto dir_list = array(curdir.dirEntries(SpanMode.shallow));
    sort!("a.name > b.name")(dir_list);

    foreach(DirEntry dir; dir_list) {
        if( dir.isFile() && dir.name.endsWith(".cmt") ) {
            auto sub = context.addSubContext("comments");
            post_type post = get_content(dir.name);
            sub["id"] = post.metadata["ID"];
            sub["author"] = post.metadata["AUTHOR"];
            auto date_time = extract_date_time(post.metadata["DATE"]);
            sub["date"] = date_time[0];
            sub["time"] = date_time[1];
            sub["text"] = post.buffer;
        }
    }

}

void view_raw(string base_dir, string [] params, HTTPServerResponse res) {
    string file_name = params[0];
    string file_path = base_dir ~ file_name;

    if( file_name.length == 0 || !file_path.exists ) {
        string error = app_cfg().dir_template ~ "error.html".readText;
        res.writeBody(error, "text/html; charset=UTF-8");
		return;
    }

    if( file_name.endsWith(".css") || file_name.endsWith(".html") || file_name.endsWith(".htm") ) {
        string mime_type = "text/html; charset=UTF-8";
        string buffer = file_path.readText;
        res.writeBody(buffer, mime_type);
    } else {

        string extension = file_name.file_extension.toLower;
        string mime;
        if(extension in mime_types) {
            mime = mime_types[extension];
        } else {
            mime = "text/" ~ extension;
        }

        if(mime == "") {
            //FIXME: mime not found. should we log ?
            mime = null;
        }

        res.writeBody(cast(ubyte[]) file_path.read, mime);
    }
}

bool check_size(char [] line, string templ) {
    ulong sz = templ.length;
    if( line.length < sz ) return false;
    if( line[0 .. sz] == templ ) return true;
    return false;
}

string get_item(char [] line, string templ) {
    return line[ templ.length+1 .. $-1 ].dup;
}

post_item get_title(string base_dir, string file_name) {

    string id_templ = "[ID]: # ";
    string date_templ = "[DATE]: # ";
    post_item p;

    p.file_name = file_name[base_dir.length..$-3];

    auto range = File(file_name).byLine();
    foreach(line; range) {
        if( line.length > 1 && line[0] == '#' && line[1] != '#' ) {
            p.title = line[1..$].dup;
        } else if( check_size(line, id_templ) ) {
            p.id = get_item(line, id_templ);
        } else if( check_size(line, date_templ) ) {
            p.date = get_item(line, date_templ);
        } else {
            if( line == "<!-- more -->" ) {
                break;
            }
            p.summary ~= line ~ "\n";
        }
    }
    return p;
}

void list_posts(string base_dir, HTTPServerResponse res) {

    Mustache mustache;
    mustache.ext("tpl");
    auto context = new Mustache.Context;

    //Ordering from the newest to the oldest
    auto dir_list = array(base_dir.dirEntries(SpanMode.shallow));
    sort!("a.name > b.name")(dir_list);

    foreach(DirEntry dir; dir_list) {
        if( dir.isFile() && dir.name.endsWith(".md") ) {
            auto sub = context.addSubContext("post_list");

            post_item p = get_title(base_dir, dir.name);
            sub["file_name"] = app_cfg().view_pattern ~ "/" ~ p.file_name;
            sub["title"] = p.title;
            auto date_time = extract_date_time(p.date);
            sub["date"] = date_time[0];
            sub["time"] = date_time[1];
            sub["summary"] = render_md(p.summary);
        }
    }

    string page_content = mustache.render(app_cfg().dir_template ~ "postlist", context);
    res.writeBody(page_content, "text/html; charset=UTF-8");

}

string render_md(string buffer) {
    import thiagocafe.markdown : filterMarkdown;
    import thiagocafe.markdown : MarkdownFlags;

    return filterMarkdown(buffer, MarkdownFlags.backtickCodeBlocks);
}
