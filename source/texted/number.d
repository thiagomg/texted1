module texted.number;

import std.stdio;
import std.random;
import std.typecons;
import std.conv;

string[int] numbers;
string[char] operations;

shared static this() {
    numbers[0] = "zero";
    numbers[1] = "one";
    numbers[2] = "two";
    numbers[3] = "three";
    numbers[4] = "four";
    numbers[5] = "five";
    numbers[6] = "six";
    numbers[7] = "seven";
    numbers[8] = "eight";
    numbers[9] = "nine";

    operations['+'] = "addition";
    operations['*'] = "multiplication";
    operations['-'] = "subtraction";
}

auto get_op(int iop) {
    switch(iop) {
    case 0:
        return tuple("+", true);
    case 1:
        return tuple("-", true);
    case 2:
        return tuple("*", true);
    case 3:
        return tuple(operations['+'], false);
    case 4:
        return tuple(operations['-'], false);
    case 5:
        return tuple(operations['*'], false);
    default:
        return tuple("+", true);
    }
}

auto resolve(int iop, int n1, int n2) {
    if(n1>9) n1 -= 10;
    if(n2>9) n2 -= 10;

    switch(iop) {
    case 0:
    case 3:
        return n1+n2;
    case 1:
    case 4:
        return n1-n2;
    case 2:
    case 5:
        return n1*n2;
    default:
        return n1+n2;
    }
}

string get_number(int n) {

    if(n < 10) {
        return numbers[n];
    }

    return to!string(n-10);
}

auto get_calc() {
    auto i = uniform(0, 20);
    auto j = uniform(0, 20);
    string n1 = get_number(i);
    string n2 = get_number(j);

    auto iop = uniform(0, 6);
    auto op = get_op(iop);

    string sentence;
    int result;
    if(op[1]) {
        sentence = n1 ~ " " ~ op[0] ~ " " ~ n2;
        result = resolve(iop, i, j);
    } else {
        sentence = op[0] ~ " of " ~ n1 ~ " and " ~ n2;
        result = resolve(iop, i, j);
    }
    return tuple!("sentence", "result")(sentence, result);
}

// void main()
// {
//     for(int i=0; i < 30; ++i) {
//         auto calc = get_calc();
//         writeln(calc.sentence, " = ", calc.result);
//     }
// }
