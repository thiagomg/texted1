module texted.config_holder;

import config.reader;

//Make it a singleton
static config_data _config_data;
static app_cfg_data _cfg;

class app_cfg_data {

    const string view_pattern;
    const string img_pattern;
    const string public_pattern;
    const string posts_pattern;
    const string comments_pattern;
    const string human_pattern;
    const string[] domains;

    const string dir_posts;
    const string dir_posts_img;
    const string dir_comments;
    const string dir_template;
    const string dir_public;
    const string dir_usage_data;

    this(config_data _config) {
        synchronized {
            view_pattern = _config.get_s("url.view.pattern");
            img_pattern = _config.get_s("url.img.pattern");
            public_pattern = _config.get_s("url.public.pattern");
            posts_pattern = _config.get_s("url.posts.pattern");
            comments_pattern = _config.get_s("url.comments.pattern");
            human_pattern = _config.get_s("url.human.pattern");

            domains = _config.get!(string[])("url.name");

            // add / at the end if not present
            dir_posts = _config.get_s("dir.posts");
            dir_posts_img = _config.get_s("dir.posts.img");
            dir_comments = _config.get_s("dir.comments");
            dir_template = _config.get_s("dir.template");
            dir_public = _config.get_s("dir.public");
            dir_usage_data = _config.get_s("dir.usage-data");
        }
    }

}

shared static this() {
    read_config(_config_data, "web.cfg");
    _cfg = new app_cfg_data(_config_data);
}

static app_cfg_data app_cfg() {
    return _cfg;
}

static config_data get_config() {
    return _config_data;
}
