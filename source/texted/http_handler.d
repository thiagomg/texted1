module texted.http_handler;

import vibe.d;

import texted.stats;
import texted.renderer;
import texted.human;
import texted.comment;

import texted.utils;
import std.stdio;
import std.algorithm;

import texted.config_holder;

void add_stat(HTTPServerRequest req) {
    add_view(req.path, req.headers.get("Referer", "DIRECT"), req.clientAddress.toString, req.timeCreated);
}

string handle_referer(HTTPServerRequest req) {
    string r = req.headers.get("Referer", "DIRECT");
    string addr;

    if( r.startsWith("http://") ) {
        addr = r[7..$];
    } else if( r.startsWith("https://") ) {
        addr = r[8..$];
    } else {
        return "/";
    }

    auto addr_ar = findSplitBefore(addr, "/");

    //FIXME: Ugly code!
    bool found = false;
    foreach(string domain; app_cfg().domains) {
        //writeln(addr_ar[0], domain);
        if( addr_ar[0].endsWith(domain) ) {
            found = true;
        }
    }

    if( !found ) {
        return "/";
    }

    if( addr_ar[1].startsWith("/view/") ) {
        return r;
    }

    return "/";
}


void handler(HTTPServerRequest req, HTTPServerResponse res)
{
    string s = req.path;
    string [] items = split(s, "/");

    add_stat(req);

    //At least 3 items -> first empty, second command, thrird+ parameters
    if( items.length < 3 ) {
        //Check if it's root (thiagocafe.com/)
        if( items.length == 2 && items[0].length == 0 && items[1].length == 0 )
            list_posts(app_cfg().dir_posts, res);
        else
            res.redirect("/");

        return;
    }

    if(items[1] == app_cfg().view_pattern) {
        view(app_cfg().dir_posts, items[2..$], app_cfg().dir_comments, req, res);
    } else if(items[1] == app_cfg().human_pattern) {
        human(items[2..$], req, res);
    } else if(items[1] == app_cfg().img_pattern) {
        view_raw(app_cfg().dir_posts_img, items[2..$], res);
    } else if(items[1] == app_cfg().public_pattern) {
        if( items.length >= 2 )
            view_raw(app_cfg().dir_public, items[2..$], res);
	} else if(items[1] == app_cfg().comments_pattern) {
        if( req.method == HTTPMethod.POST ) {
            string referer = handle_referer(req);
            comment(app_cfg().dir_comments, items[2..$] ~ referer, req, res);
        } else {
            res.writeBody("Unknown comment: " ~ req.path, "text/html; charset=UTF-8");
        }
    } else {
        res.writeBody("Unknown path: " ~ req.path, "text/html; charset=UTF-8");
    }

}
